* Conceito de cultura
** Resumo

   O artigo compara duas definições de "cultura", ressaltando suas
   diferenças e propõe uma sistematização do conceito.

** Anotações
*** O conceito de cultura em Terry Eagleton (Livro: "A ideia de cultura")
**** Citação

     #+BEGIN_SRC sh

     "O ator aponta a necessidade de de problematizar o conceito frente
     seu desgaste e utilização indevida em várias áreas de
     conhecimento."

     #+END_SRC
------

**** Citação

     #+BEGIN_SRC sh

     "[...] o desafio é mostrar que não é possível que a análise sobre
     cultura exclua as relações sobre a reprodução material da vida.
     Assim, lidar com o conceito de cultura [...], é lidar com um conceito
     histórico, e em movimento."

     #+END_SRC
------

**** Citação

     #+BEGIN_SRC sh

     "[...] propõe uma análise de processo histórico de modo que se
     perceba a mudança histórica e, com ela, a mudança de significado
     que os fatos possuem para os sujeitos que os vivenciam em cada
     período."

     #+END_SRC

------

**** Citação

     #+BEGIN_SRC sh

     "A cultura é então simplesmente tudo que não é geneticamente
     transmissível. [...] Desde a década de 1960, entretanto, a palavra
     'cultura' foi girando sobre seu eixo até significar exatamente o
     oposto. Ela agora significa a afirmação de uma identidade
     específica - nacional, sexual, étnica, regional - em vez da
     transcendência desta. E já que essas identidades todas vêem a si
     mesmas como oprimidas, aquilo que era antes concebido como um
     reino de consenso foi transformado em um terreno de conflito.
     Cultura, em resumo, deixou de ser parte da solução para ser parte
     do problema."

     - Eagleton, 2005, p. 54

     #+END_SRC

***** Pensamentos

      De fato, mesmo com as visões mais amplas citadas a respeito do conceito
      de cultura, esta citação em específico foi recebida com
      estranheza em meus pensamentos, pois, instintivamente, o
      conceito de idêntidade e cultura estão entrelaçados na minha
      concepção.

------

**** Citação

     #+BEGIN_SRC sh

     "A noção de liberdade no pós-modernismo se transformou em
     sinônimo de liberdade pessoal, implicando na insignificância de
     instituiçpões historicamente conquistadas coletivamente, como
     sindicatos, movimentos sociais e outras organizações."

     #+END_SRC

***** Pensamento

      De fato, nos pensamentos mais opostos ao viés marxista, o
      indivíduo é o alvo de exaltação, e não o coletivo, sendo o
      "senso de coletividade" originado do "isolamento" em sua
      essência única e individual de cada membro da sociedade.

------

**** Citação

     #+BEGIN_SRC sh

     "O Mercado deixou de ser compreendido enquanto uma relação
     social, numa tentativa de despolitizar uma relação que é
     altamente política, através da procriação de elementos
     identitários (moda para negros, obesos etc...)
     [...]
     No mercado, são assumidas determinadas relações que não se tratam
     de relações transcendentes, sem sujeitos, porém é como se o
     fosse."

     #+END_SRC

***** Pensamento

      Seria o "Mercado financeiro" um exemplo mais direto deste
      fenômeno de despolitização?

------

**** Citação

     #+BEGIN_SRC sh

     "O pós-modernismo, ao inaugurar a linguagem do discurso de
     identidade, trouxe junta à despolitização da noção de classe uma
     outra espécie de politização desprovida da luta e da compreensão
     do funcionamento da sociedade capitalista de forma ampla. Assim,
     o pós-modernismo realiza um deslocamento do conflito de classe
     não indo além da constatação da existência de uma diversidade
     atrelada unicamente a uma questão de identidade."

     #+END_SRC

***** Pensamento

      Em um tom bem informal e pessoal aqui: Pelo visto preciso ler os
      textos base de Marx, pois políticas de identidade, na minha
      ignorância, faziam parte do pensamento marxista.

      Talvez sejam parte do pensamento marxista "pós-moderno"?

------

**** Citação

     #+BEGIN_SRC sh

     "Para Eagleton, se a cultura reflete toda uma trajetória, uma
     mudança semântica no conceito, o que então estaria em crise é o
     pensamento pós-moderno que tenta homogeneizar as relações sociais
     contruídas de modo complexo."

     #+END_SRC

------

*** O conceito de cultura em Simmel (Ensaio: O conceito e a tragédia da cultura)
**** Citação

     #+BEGIN_SRC sh

     "Para Simmel, o processo cultural é uma dialética entre sujeito e
     objeto [...]"

     #+END_SRC

------

**** Citação

     #+BEGIN_SRC sh

     "[...] Para o autor a cultura estaria sempre fluindo, fazendo com
     que a vida se objetive a partir das formas [...] Assim, a vida é
     entendida como um fluir permanente de modo amplo, Para Simmel, a
     vida e a forma precisam estar constantemente produzindo efeitos
     de subjetivação. [...] Nesta direção, para o autor o conceito de
     cultura é instituído, [...] sendo as as características das
     formas culturais a cognição e a afetividade."

     #+END_SRC

***** Pensamento

      "Subjetivação" aqui é usado no sentido de "individualizar" algo.

------

**** Citação

     #+BEGIN_SRC sh

      "[...], formas são roupagens que se dá a vida."

     #+END_SRC

***** Pensamento

      Nessa definição, "forma" é uma lente de percepção que um
      indivíduo tem sobre a sua realidade?

------

**** Citação

     #+BEGIN_SRC sh

     "A forma e o estilo são maneiras genéricas de dar forma, o que
     para Simmel vêm se tornando efêmeras provocando assim, a
     impossibilidade de subjetivação, o que por sua vez origina um
     processo de desumanização."

     #+END_SRC

***** Pensamento

      As formas de subjetivação estão se tornando muito banais(?) e,
      como para o autor não há cultura sem subjetivação, isso gera um
      processo de desumanização?

------

**** Citação

     #+BEGIN_SRC sh

     "Nela (na modenernidade), o homem tem dificuldade de ver sua
     própria imagem. Os aspectos culturais para o autor (arte,
     aspectos sociais, econômicos, etc...) não produzem mais
     subjetivação."

     #+END_SRC

------

**** Citação

     #+BEGIN_SRC sh

     "[...] A abordagem crítica da autora (Claudine Haroche) se volta
     ao declínio e a atrofia das maneiras de perceber e de sentir, o
     que tem como decorrência a relatividade no ato de pensar. Discute
     sobre o fluxo de sensações intensificado pelas mídias, que vai
     afetar a percepção, os afetos e as atividades de pensar na psique
     contemporânea."

     #+END_SRC

***** Pensamento

      Saia das redes sociais.

------

**** Citação

     #+BEGIN_SRC sh

     "Aos objetos que tem significados, mas que não são significantes,
     Simmel denomina de tragédia. [...] A tragédia que menciona
     Simmel, não é exterior, o capitalismo, ou o consmismo, mas é a
     lógica interna, dos dispositivios, de sua rede e os efeitos que
     estaria provocando. A lógica interna faz com que as coisas se
     transformem sempre no sentido da mesmice, sem lugar para o
     imprevisível e para mudanças qualitativas. As formas culturais se
     renovam com a inovação tecnológica, por exemplo, (celulares), mas
     essa inovação ocorre na mesmice."

     #+END_SRC

***** Pensamento

      O incrível avanço tecnológico no que diz respeito a
      conectividade e na globalização do contato, tem seu pesares,
      quando pensamos no efeito das redes sociais.

------

*** TODO O conceito de cultura e as evidências históricas
    A ser adicionado...

------

** Notas pontuais
*** Sobre Terry Eagleton em "A ideia de cultura":

    - Eagleton faz uma análise sobre o conceito de cultura a partir de
      diversos ângulos, apontando tanto a fundamentação quanto os
      limites de cada.
    - O autor tem um problema em separar as questões históricas e
      filosóficas na definição de cultura, pois a abstração do ponto
      de vista filosófico se distancia muito do material, e isto,
      escecificamente quando se tratando do conceito de cultura, é
      ruim.
    - Para Raymond Williams, mesmo que exista uma chamada "estrutura"
      (cultural?), esta encontra-se sempre em um estado dinâmico,
      sendo constituinda de pressões (da sociedade? )e limites (do
      invíduo) daquele que vivencia a realidade.
    - Talvez seja correto dizer que para Eagleton, a não
      contextualização do conceito de cultura é bobagem.
    - Eagleton parece ter um problema sério com o pós-modernismo.

*** Sobre Simmel em "O conteito e a tragédia da cultura"

    - Haroche: a autora tem uma boa (e dura) análise crítica sobre o
      cenário moderno, tendo em vista, o excesso de carga midiática e
      os canais de comunicação. Boa leitura.
